package fr.umfds.td1bis;

import java.util.HashMap;
import java.util.*;

public class groupe {
	private int id;
	private String nom;
	private List<etudiant> membreGroupe = new ArrayList<>();
	private HashMap<Integer,voeux> choixVoeux = new HashMap<>(); 
	private int i = 1;
	private sujet affectation;

	public groupe(int id, String nom) {
		this.id = id;
		this.nom = nom;
	}
	
	public void addToGroupe(etudiant e) {
		if((membreGroupe.size()<5) && (!(membreGroupe.contains(e)))) {
			membreGroupe.add(e);
			e.setAffectation(this);
		}
	}
	
	public void faireVoeux(voeux v) {
		if((i < 5) && (!(choixVoeux.containsValue(v)))) {
			choixVoeux.put(i, v);
			i++;
		}
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<etudiant> getMembreGroupe() {
		return membreGroupe;
	}

	public HashMap<Integer, voeux> getChoixVoeux() {
		return choixVoeux;
	}

	public sujet getAffectation() {
		return affectation;
	}

	public void setAffectation(sujet affectation) {
		this.affectation = affectation;
	}


	
	
}
