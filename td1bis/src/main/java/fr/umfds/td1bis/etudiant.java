package fr.umfds.td1bis;

public class etudiant extends personne {
	private int numEt;
	private groupe affectation;

	public etudiant(String n, String p, String m, int e) {
		super(n, p, m);
		numEt = e;
	}

	public int getNumEt() {
		return numEt;
	}
	
	public groupe getAffectation() {
		return affectation;
	}

	public void setNumEt(int numEt) {
		this.numEt = numEt;
	}
	
	public void setAffectation(groupe affectation) {
		this.affectation = affectation;
	}
	
	
}
