package fr.umfds.td1bis;

public class sujet {
	private String titre;
	private String resume;
	private enseignant encadrant;
	private groupe groupeAffecte;
	private boolean affecte  = false;

	public sujet(String titre, String resume, enseignant encadrant) {
		this.titre = titre;
		this.resume = resume;
		this.encadrant = encadrant;
	}
	
	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getResume() {
		return resume;
	}

	public void setResume(String resume) {
		this.resume = resume;
	}

	public enseignant getEncadrant() {
		return encadrant;
	}

	public void setEncadrant(enseignant encadrant) {
		this.encadrant = encadrant;
	}
	
	public groupe getGroupeAffecte() {
		return groupeAffecte;
	}

	public void setGroupeAffecte(groupe groupeAffecte) {
		this.groupeAffecte = groupeAffecte;
		this.affecte = true;
	}
	
	public boolean isAffecte() {
		return this.groupeAffecte != null;
	}

}
