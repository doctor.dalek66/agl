package fr.umfds.td1bis;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;

public class AppTest {
	
	etudiant e1 = new etudiant("e","1","e.1@etu.umontpellier.fr",218);
	etudiant e2 = new etudiant("e","2","e.2@etu.umontpellier.fr",228);
	etudiant e3 = new etudiant("e","3","e.3@etu.umontpellier.fr",238);
	etudiant e4 = new etudiant("e","4","e.4@etu.umontpellier.fr",248);
	etudiant e5 = new etudiant("e","5","e.5@etu.umontpellier.fr",258);
	enseignant ee1 = new enseignant("e","e1","ee.1@etu.umontpellier.fr");
	sujet s1 = new sujet("...", null, ee1); voeux v1 = new voeux(s1);
	sujet s2 = new sujet("...", null, ee1); voeux v2 = new voeux(s2);
	sujet s3 = new sujet("...", null, ee1); voeux v3 = new voeux(s3);
	sujet s4 = new sujet("....", null, ee1); voeux v4 = new voeux(s4);
	sujet s5 = new sujet("...", null, ee1); voeux v5 = new voeux(s5);
	sujet s6 = new sujet("...", null, ee1); voeux v6 = new voeux(s6);
	sujet s7 = new sujet("...", null, ee1); voeux v7 = new voeux(s7);
	groupe g1 = new groupe(1,"1d6");
	groupe g2 = new groupe(2,"2d7");
	
	@BeforeEach
	void init()  {
		  g1.addToGroupe(e1);g1.addToGroupe(e2);g1.addToGroupe(e3);
		  g2.addToGroupe(e4);g2.addToGroupe(e5);
		  g1.faireVoeux(v1); g1.faireVoeux(v2); g1.faireVoeux(v3); g1.faireVoeux(v4);
		  g2.faireVoeux(v7); g2.faireVoeux(v6); g2.faireVoeux(v5); g2.faireVoeux(v4); g2.faireVoeux(v3);
		  g1.setAffectation(s1);
		  s1.setGroupeAffecte(g1);
	}
	
	@Test
	public void G1dansS1() {
		assertEquals(s1.getGroupeAffecte(),g1);
	}
	
	@Test
	public void S1dansG1() {
		assertEquals(g1.getAffectation(),s1);
	}
	
	@Test
	public void S1estAffecte() {
		assertTrue(s1.isAffecte());
	}


}
