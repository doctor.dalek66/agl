package fr.umfds.tp3;

public class MailIncorrectException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MailIncorrectException(String string) {
		super(string);
	}
}
