package fr.umfds.tp3;

import java.util.ArrayList;
import java.util.regex.*;

public class Courriel {
	private String destination;
	private String titre;
	private String corps;
	private ArrayList<String> piecesJointes;
	
	public boolean verifAdresse (String s){
		Pattern pattern = Pattern.compile("[A-z]+.*[@][A-z]+[.][A-z]+");
		Matcher m = pattern.matcher(s);
		return m.find();
	}
	
	public Courriel(String destination, String titre, String corps)/*throws MailIncorrectException*/{
		/*if(!verifAdresse(destination)) {
			throw new MailIncorrectException("mail incorrect");
		}*/
		this.setDestination(destination);
		this.setTitre(titre);
		this.setCorps(corps);
		this.piecesJointes = new ArrayList<String>();
		/*if(!verifAdresse(destination)) {
			throw new MailIncorrectException("incorrect");	
		}*/
	}
	
	public void add(String s) {
		piecesJointes.add(s);
	}
	

	public String getCorps() {
		return corps;
	}

	public void setCorps(String corps) {
		this.corps = corps;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	
	
	
}
