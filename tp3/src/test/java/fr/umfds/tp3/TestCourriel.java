package fr.umfds.tp3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
//import org.junit.jupiter.params.provider.Arguments;
import java.util.stream.Stream;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.function.Executable;
//import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;

public class TestCourriel {
	static Courriel c1 = new Courriel("florian.ravet-lecourt@etu.umontpellier.fr","bonjour","je veux tester courriel.java");
	static Courriel c2 = new Courriel("16436jaipasdearobase.etu.umontpellier.fr","bonjour","je veux tester courriel.java");

	
	@DisplayName("vérification de mails")
	@ParameterizedTest(name = "{index} : destination = {0}, raison = {1}")
	@MethodSource("mailProvider")
	void verifMail(String s, String r) {
		assertFalse(c1.verifAdresse(s),r);
		//faire une version avec setDestination()
	}

	private static Stream<Arguments> mailProvider(){
		return Stream.of(
				Arguments.of(c2.getDestination(),"pas de @"),
				Arguments.of("","vide"),
				Arguments.of("4646gsjgos@gijqs.-*-/-*/-/-/*-//-/-/-/-/-/.com","commence par un chiffre")
				);
	}
	
	@ParameterizedTest
	@ValueSource(strings = {"16436jaipasdearobase.etu.umontpellier.fr","","4646gsjgos@gijqs.-*-/-*/-/-/*-//-/-/-/-/-/.com"})
	void testMail(String s) {
		assertFalse(c1.verifAdresse(s));
	}
	
	
	
	
	/*@Test
	public void verifAdresseC1ShouldBeTrue() {
		assertTrue(c1.verifAdresse(c1.getDestination()));
	}
	
	@Test
	public void verifAdresseC2ShouldBeFalse() {
		assertFalse(c2.verifAdresse(c2.getDestination()));
	}
	
	@Test
	public void titreC1ShouldBeTrue() {
		assert(c1.getTitre() != null);
	}
	
	@Test
	public void titreC2ShouldBeTrue() {
		assert(c2.getTitre() != null);
	}*/


}
