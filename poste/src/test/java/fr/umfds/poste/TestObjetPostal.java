package fr.umfds.poste;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestObjetPostal {
	private static float tolerancePrix=0.001f;
	private static float toleranceVolume=0.0000001f;
	
	Lettre lettre1 = new Lettre("Le pere Noel",
			"famille Kirik, igloo 5, banquise nord",
			"7877", 25, 0.00018f, Recommandation.un, false);
	Lettre lettre2 = new Lettre("Le pere Noel",
			"famille Kouk, igloo 2, banquise nord",
			"5854", 18, 0.00018f, Recommandation.deux, true);
	Lettre lettre3 = new Lettre("Le pere Noel",
			"famille Kouk, igloo 2, banquise nord",
			"5854", 18, 0.00018f, Recommandation.zero, true);
	Colis colis1 = new Colis("Le pere Noel", 
			"famille Kaya, igloo 10, terres ouest",
			"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);
	Colis colis2 = new Colis("Le pere Noel", 
			"famille Kaya, igloo 10, terres ouest",
			"7877", 1024, 0.02f, Recommandation.zero, "train electrique", 200);
	Colis colis3 = new Colis("Le pere Noel", 
			"famille Kaya, igloo 10, terres ouest",
			"7877", 1024, 0.02f, Recommandation.un, "train electrique", 200);
	Colis colis4 = new Colis("Le pere Noel", 
			"famille Kaya, igloo 10, terres ouest",
			"7877", 1024, 0.6f, Recommandation.un, "train electrique", 200);
	SacPostal sac1 = new SacPostal();
	SacPostal sac3 = new SacPostal();

	@BeforeEach
	void init() throws ColisExpressInvalide {
		sac1.ajoute(lettre1);
		sac1.ajoute(lettre2);
		sac1.ajoute(colis1);
		Lettre lettre4 = new Lettre();
	}
	
	@Test
	void testToStringLettre1() {
		assertEquals("Lettre 7877/famille Kirik, igloo 5, banquise nord/1/ordinaire",lettre1.toString());
	}
	
	@Test
	void testToStringLettre2() {
		assertEquals("Lettre 5854/famille Kouk, igloo 2, banquise nord/2/urgence",lettre2.toString());
	}
	
	@Test
	void testToStringColis1() {
		assertEquals("Colis 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0",colis1.toString());
	}
	
	@Test
	  void affranchissementLettre1() {
	    assertTrue(Math.abs(lettre1.tarifAffranchissement()-1.0f)<tolerancePrix);
	    assertFalse(Math.abs(lettre1.tarifAffranchissement()-4.0f)<tolerancePrix);
	  }
	
	@Test
	  void affranchissementLettre2() {
	    assertTrue(Math.abs(lettre2.tarifAffranchissement()-2.3f)<tolerancePrix);
	  }
	
	@Test
	  void affranchissementColis1() {
	    assertTrue(Math.abs(colis1.tarifAffranchissement()-3.5f)<tolerancePrix);
	  }
	
	@Test
	  void remboursementLettre1() {
	    assertTrue(Math.abs(lettre1.tarifRemboursement()-1.5f)<tolerancePrix);
	  }
	
	@Test
	  void remboursementLettre2() {
	    assertTrue(Math.abs(lettre2.tarifRemboursement()-15.0f)<tolerancePrix);
	  }
	
	@Test
	  void remboursementLettre3() {
	    assertTrue(Math.abs(lettre3.tarifRemboursement())<tolerancePrix);
	  }
		
	@Test
	 void remboursementColis() {
		assertTrue(Math.abs(colis1.tarifRemboursement()-100.0f)<tolerancePrix);
		assertEquals(0, colis2.tarifRemboursement());
		//assertFalse(Math.abs(colis2.tarifRemboursement()-100.0f)<tolerancePrix);
		assertTrue(Math.abs(colis3.tarifRemboursement()-20.0f)<tolerancePrix);
	}
	
	@Test
		void typeColisAndToString() throws ColisExpressInvalide{
		ColisExpress e = new ColisExpress("bl","adr","num",29,0.01f,Recommandation.deux,"objet",198f,true);
		assertEquals("Colis express",e.typeObjetPostal());
		assertEquals("Colis express num/adr/2/0.01/198.0/29.0/0",e.toString());
	}
	
	
	@Test
	void exceptionLevee() throws ColisExpressInvalide {
		Assertions.assertThrows(ColisExpressInvalide.class, () -> {
			ColisExpress colisE = new ColisExpress("Le pere Noel", 
					"famille Artick, igloo 90, baie des vents",
					"7855", 31, 0.01f, Recommandation.deux, "nintendo switch", 198, true);
		});
	}
	
	@Test
	  void valeurRemboursmentSac1() {
	    assertTrue(Math.abs(sac1.valeurRemboursement()-116.5f)<tolerancePrix);
	}
	
	@Test
	  void volumeSac1() {
	    assertTrue(Math.abs(sac1.getVolume()-0.025359999558422715f)<toleranceVolume);
	  }
	
	@Test
		void capaciteSac() {
		assertEquals(0.5,sac3.getCapacite());
	}
	
	@Test
		void toStringSac() {
		assertEquals(sac3.toString(),"Sac \ncapacite: "+0.5+
				"\nvolume: "+0.005+"\n"+"[]"+"\n");
	}
	
	@Test
		void nePeuxPasAjouter() {
		assertFalse(sac1.ajoute(colis4));
	}
	
	@Test
	  void volumeSac2() {
		SacPostal sac2 = sac1.extraireV1("7877");
	    assertTrue(sac2.getVolume()-0.02517999955569394f<toleranceVolume);
	  }
	
	@Test
	  void volumeSac2_2() {
		SacPostal sac2 = sac1.extraireV2("7877");
	    assertTrue(sac2.getVolume()-0.02517999955569394f<toleranceVolume);
	  }

}
