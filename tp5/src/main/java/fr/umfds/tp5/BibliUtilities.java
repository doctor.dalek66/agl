package fr.umfds.tp5;

import java.util.*;
import java.util.stream.Collectors;
import java.time.Clock;
import java.time.LocalDate;

public class BibliUtilities{
	BiblioInterface gbAccess;
	private static int delaiInventaire = 12;
	private Clock clock;
	
	public BibliUtilities(GlobalBibliographyAccess m){
		super();
		gbAccess = m;
		clock = Clock.systemDefaultZone();
	}
		
	public List<NoticeBibliographique> chercherNoticesConnexes(NoticeBibliographique ref){
		ArrayList<NoticeBibliographique> res = new ArrayList<>();
		res = gbAccess.noticesDuMemeAuteurQue(ref);
		Set<String> nameSet = new HashSet<>();
		return res.stream()
				.filter(n -> !n.getTitre().equals(ref.getTitre()))
				.filter(e -> nameSet.add(e.getTitre()))
				.limit(5)
				.collect(Collectors.toList());
				
	}
	
	public NoticeStatus ajoutNotice(String isbn) throws AjoutImpossibleException{
		NoticeStatus res = null;
		try {
			NoticeBibliographique nb = gbAccess.getNoticeFromIsbn(isbn);
			res = Bibliotheque.getInstance().addNotice(nb);
		}catch(IncorrectIsbnException e) {
			throw new AjoutImpossibleException();
		}
		return res;
	}
	
	public boolean prevoirInventaire() {
		return Math.abs(LocalDate.now(clock).until(Bibliotheque.getInstance().getLastInventaire()).toTotalMonths()) > delaiInventaire;
	}
}

	