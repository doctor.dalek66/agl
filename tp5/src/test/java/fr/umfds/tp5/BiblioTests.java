package fr.umfds.tp5;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import static org.mockito.Mockito.*;

import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;

class BiblioTests {
	GlobalBibliographyAccess mock1 = mock(GlobalBibliographyAccess.class);
	BiblioInterface mock2 = mock(BiblioInterface.class);
	Clock mockedClock = mock(Clock.class);
	BibliUtilities BU = new BibliUtilities(mock1);
	
	//@ExtendWith(MockitoExtension.class)
	LocalDate uneDate = LocalDate.of(2022, 12, 01);
	Clock fixedClock = Clock.fixed(uneDate.atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault());
	NoticeBibliographique nb1 = new NoticeBibliographique ("","1","");
	NoticeBibliographique nb2 = new NoticeBibliographique ("","2","");
	ArrayList<NoticeBibliographique> list = new ArrayList<NoticeBibliographique>();
	ArrayList<NoticeBibliographique> list2 = new ArrayList<NoticeBibliographique>();
	
	@BeforeEach
	void init() {
		NoticeBibliographique nb3 = new NoticeBibliographique ("","3","");
		NoticeBibliographique nb4 = new NoticeBibliographique ("","4","");
		NoticeBibliographique nb5 = new NoticeBibliographique ("","5","");
		NoticeBibliographique nb6 = new NoticeBibliographique ("","6","");
		NoticeBibliographique nb7 = new NoticeBibliographique ("","7","");
		list.add(nb2);list.add(nb3);list.add(nb4);list.add(nb5);list.add(nb6);
		list2.add(nb2);list2.add(nb3);list2.add(nb4);list2.add(nb5);list2.add(nb6);list2.add(nb7);
	}
	
	@Test
	//@ExtendWith(MockitoExtension.class)
	void returnGoodList() {
		when(mock1.noticesDuMemeAuteurQue(nb1)).thenReturn(list);
		assertEquals(BU.chercherNoticesConnexes(nb1),list);
	}
	
	@Test
	void returnListMinusOne() {
		when(mock1.noticesDuMemeAuteurQue(nb1)).thenReturn(list2);
		assertEquals(BU.chercherNoticesConnexes(nb1),list);
	}
	

}
	
	
	
